<?php

namespace App\Models;

use Illuminate\database\Eloquent\Factories\HasFactory;
use ILluminate\database\Eloquent\Factories\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'Products';

    protected $fillable = ['nome', 'descricao', 'data', 'valor'];



}