<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $product = products::all();

        return response()->json($product);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valida = Validator::make($request->all(), [
            'name' => 'required|max:100|min:10',
            'value' => 'required',
            'description' => 'max:255|min:10',
            'data'=> 'required'
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'Sua requisição está faltando dados'
            ], 400);
        };

        $product = products::create($request->all());
        return response()->json([
            'message' => 'Produto criado com Sucesso',
            'product' => $product
        ], 201);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\products  $products
     * @return \Illuminate\Http\Response
     */
    public function show(products $products)
    {
        return response()->json
        ([
             'message'=> 'Sucesso',
            'products'=>$product
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, products $products)
    {
        $valida = Validator::make($request->all(),
        [
            'name' => 'required|max:255|min:10',
            'value' => 'required',
            'description'=>  'required|max:255|min:15'
        ]);
    
       if($valida->fails())
           {
             return response()->json 
               ([
                  'message' => ' Sua requisao esta faltando dados'   
               ], 400);           
           }
       $product->update($request->all());

       return response()->json
          ([
              'message' => 'Produto atualizado com sucesso',
              'product' => $product
          ], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(products $products)
    {
        $products->delete();
            return response()->json
                ([
                    'messagem' => 'Produto deletado com sucesso',
                ]);
    }
}
